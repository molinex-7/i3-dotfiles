"
"   °v°   O Mundo é pinguim...
"  /(_)\       .vimrc
"   ^ ^
"
set nocompatible

"******************************************************************************
" Load config files
"******************************************************************************

source ~/.vim/scripts/vimscripts/vundle_config.vim
source ~/.vim/scripts/vimscripts/std_config.vim
source ~/.vim/scripts/vimscripts/generate_ctags.vim
source ~/.vim/scripts/vimscripts/omnicpp_config.vim
source ~/.vim/scripts/vimscripts/airline_config.vim
source ~/.vim/scripts/vimscripts/nerdtree_config.vim
source ~/.vim/scripts/vimscripts/ultisnips_config.vim
source ~/.vim/scripts/vimscripts/syntastic_config.vim
source ~/.vim/scripts/vimscripts/myvimgrep_config.vim
source ~/.vim/scripts/vimscripts/key_mapping.vim
