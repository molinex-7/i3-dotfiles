(function () {
	// Default
	//
	// https://start.duckduckgo.com/
	//
	//
	var str;
	var duck = "https://duckduckgo.com/?q="
	var go = "&ia=web"
	var form = document.getElementsByTagName("form")[0];
	var s = document.getElementById('look');

	function search(e) {
		e.preventDefault();

		if (s.value != "") {
			str = encodeURIComponent(s.value);
			location = duck + str + go;
		}
	}

	s.focus();
	form.addEventListener("submit", search);
})();

