b=$(tput bold)
r=$(tput sgr0)
PS1='\[$b\][\u@\h \W]\$\[$r\] '
PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

alias xcc="xclip -selection clipboard"
alias xcp="xclip -o -selection clipboard"
alias dot="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias tfh="$HOME/.config/i3/bin/term_from_here"
alias yay="yay; pacman -Qqn > $HOME/.notes/native_packages; pacman -Qqm > $HOME/.notes/external_packages"

export EDITOR="/usr/bin/vim"

source /usr/share/fzf/completion.bash && source /usr/share/fzf/key-bindings.bash
export FZF_DEFAULT_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
complete -o bashdefault -o default -F _fzf_path_completion zathura

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
