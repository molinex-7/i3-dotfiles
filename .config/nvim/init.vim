"
"   °v°   O Mundo é pinguim...
"  /(_)\       .nvim/init.vim
"   ^ ^
"
set nocompatible
filetype off

"******************************************************************************
" Load config files
"******************************************************************************

source ~/.config/nvim/scripts/plug_config.vim
source ~/.config/nvim/scripts/std_config.vim
source ~/.config/nvim/scripts/keymap_config.vim
source ~/.config/nvim/scripts/airline_config.vim
source ~/.config/nvim/scripts/nerdtree_config.vim
source ~/.config/nvim/scripts/emmet_config.vim
source ~/.config/nvim/scripts/ale_config.vim
source ~/.config/nvim/scripts/ultisnips_config.vim
source ~/.config/nvim/scripts/vdebug_config.vim
source ~/.config/nvim/scripts/phpactor_config.vim
source ~/.config/nvim/scripts/deoplete_config.vim
source ~/.config/nvim/scripts/mygrep_config.vim
source ~/.config/nvim/scripts/vrc_config.vim
