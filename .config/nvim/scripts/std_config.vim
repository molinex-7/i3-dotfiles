"******************************************************************************
" Std Configs
"******************************************************************************

set encoding=utf-8
set nu
set showcmd
set textwidth=80
set cursorline

" Search configs
set hlsearch
set incsearch
set ignorecase
set smartcase

" Tab configs
filetype plugin indent on
set tabstop=4 shiftwidth=4
set expandtab
set backspace=indent,eol,start

" Synthax config
syntax on
colorscheme cyan
hi Normal guibg=NONE ctermbg=NONE
