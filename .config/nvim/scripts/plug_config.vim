"******************************************************************************
" Plug Configs
"******************************************************************************

call plug#begin()

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'raimondi/delimitmate'
Plug 'grvcoelho/vim-javascript-snippets'
Plug 'othree/html5.vim'
Plug 'mattn/emmet-vim'
Plug 'shawncplus/phpcomplete.vim'
Plug 'phpactor/phpactor', {'for': 'php', 'do': 'composer install'}
Plug 'Shougo/deoplete.nvim'
Plug 'kristijanhusak/deoplete-phpactor'
Plug 'daniellesucher/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'majutsushi/tagbar'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'w0rp/ale'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'vim-vdebug/vdebug'
Plug 'diepm/vim-rest-console'

call plug#end()
