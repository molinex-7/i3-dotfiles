"******************************************************************************
" PHPActor Configs
"******************************************************************************

autocmd FileType php setlocal omnifunc=phpactor#Complete
let g:phpactorOmniAutoClassImport = v:true
let g:phpactorCompletionIgnoreCase = 0
let g:phpactorOmniError = v:true
