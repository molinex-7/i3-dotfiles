"******************************************************************************
" mygrep Configs
"******************************************************************************

augroup myvimrc
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    lwindow
    autocmd FileType qf nnoremap <buffer> <CR> <CR>:cclose<CR>
augroup END

command! -nargs=+ Gr execute 'silent gr! -Rn --exclude=*.{phar,js,json,xml} --exclude-dir={vendor,storage,node_modules,nusoap-*} <args> *'
