"******************************************************************************
" vrc Configs
"******************************************************************************

let g:vrc_trigger = '<leader>h'
let g:vrc_curl_opts = {
            \ '-sS' : '',
            \ '--connect-timeout' : 10,
            \ '-L': '',
            \ '--max-time': 60,
            \ '--ipv4': '',
            \ '-k': '',
            \}
let g:vrc_response_default_content_type = 'application/json'
let g:vrc_output_buffer_name = '__VRC_OUTPUT.json'

autocmd BufRead *.http set filetype=rest
