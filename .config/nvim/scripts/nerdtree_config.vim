""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree Configs
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:NERDTreeWinSize=35

let NERDTreeShowHidden=1
let NERDTreeNodeDelimiter="\u263a" " smiley face
let g:NERDTreeIndicatorMapCustom={
	\'Modified'  : '✹',
	\'Staged'    : '✚',
	\'Untracked' : '✭',
	\'Renamed'   : '➜',
	\'Unmerged'  : '═',
	\'Deleted'   : '✖',
	\'Dirty'     : '✗',
	\'Clean'     : '✔︎',
	\'Ignored'   : '☒',
	\'Unknown'   : '?'
    \}
