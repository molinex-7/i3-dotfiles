"******************************************************************************
" Keymap Configs
"******************************************************************************

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Toggle NerdTree
map <C-n> :NERDTreeToggle<CR>
map <leader>n :NERDTreeFind<CR>

" space open/closes folds
nnoremap <space> za

" Mappings to access buffers (don't use "\p" because a
" delay before pressing "p" would accidentally paste).
" \l       : list buffers
" \b \f \g : go back/forward/last-used
" \d       : erase buffer
" \1 \2 \3 : go to buffer 1/2/3 etc
nnoremap <Leader>l :ls<CR>
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>f :bn<CR>
nnoremap <Leader>d :bd<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>
" It's useful to show the buffer number in the status line.
set laststatus=2 statusline=%02n:%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

" TagBar
nmap <C-T> :TagbarToggle<CR>
nmap <C-F> :Files<CR>

" Phpactor re-mappings
" Include use statement
nmap <leader>u :call phpactor#UseAdd()<cr>

" Invoke the context menu
nmap <leader>mm :call phpactor#ContextMenu()<cr>

" Find references
nmap <leader>fr :call phpactor#FindReferences()<cr>

" Invoke the navigation menu
nmap <leader>nn :call phpactor#Navigate()<cr>

" Goto definition of class or class member under the cursor
nmap <leader>o :call phpactor#GotoDefinition()<cr>

" Transform the classes in the current file
nmap <leader>tt :call phpactor#Transform()<cr>

" Generate a new class (replacing the current file)
nmap <leader>cc :call phpactor#ClassNew()<cr>

" Extract expression (normal mode)
nmap <silent><leader>ee :call phpactor#ExtractExpression(v:false)<cr>

" Extract expression from selection
vmap <silent><leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<cr>

" Extract method from selection
vmap <silent><leader>em :<C-U>call phpactor#ExtractMethod()<cr>

" Vim-fugitive settings
nmap <leader>gsp :Gsplit<cr>
nmap <leader>gvsp :Gvsplit<cr>
nmap <leader>gd :Gdiff<cr>
nmap <leader>gb :Gblame<cr>
nmap <leader>gs :Gstatus<cr>
nmap <leader>gc :Git commit -m ""
nmap <leader>ga :Git add .<cr>
nmap <leader>gp :Git push origin

"grep
nmap <leader>s :Gr <c-r>=expand("<cword>")<cr><cr>
