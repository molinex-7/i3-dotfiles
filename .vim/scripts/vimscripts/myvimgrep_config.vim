"******************************************************************************
" myvimgrep
"******************************************************************************

augroup myvimgrep
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    lwindow
    autocmd FileType qf nnoremap <buffer> <CR> <CR>:cclose<CR>
augroup END

if isdirectory("src") && isdirectory("hdr")
    command! -nargs=+ Gr execute 'silent gr! -Rn <args> {src,hdr}/*'
else
    command! -nargs=+ Gr execute 'silent gr! -Rn <args> *'
endif
