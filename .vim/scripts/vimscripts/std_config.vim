"******************************************************************************
" Std Config
"******************************************************************************

set encoding=utf-8
set nu
set showcmd
set textwidth=80
set cursorline

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase

" Tab
set tabstop=4 shiftwidth=4
set expandtab
set backspace=indent,eol,start

" Syntax
syntax on
colorscheme cyan
hi Normal guibg=NONE ctermbg=NONE

" It's useful to show the buffer number in the status line.
set laststatus=2 statusline=%02n:%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
