"******************************************************************************
" Key Mapping
"******************************************************************************

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" go to definition
nnoremap <leader>o <C-]>
nnoremap <leader>O <C-T>

" open header file
nnoremap <leader>i <C-W>i

" preview tag
nnoremap <leader>p <Esc>:exe "ptjump " . expand("<cword>")<Esc>

" compile
nnoremap <F4> :exec "!make"<cr>
nnoremap <F5> :exec "!make install clean"<cr>

" run
nmap <F6> :exec "!~/.vim/scripts/shellscripts/run_prog.sh"<cr>

" NerdTree
map <C-n> :NERDTreeToggle<CR>
map <leader>n :NERDTreeFind<CR>

" space open/closes folds
nnoremap <space> za

" Mappings to access buffers (don't use "\p" because a
" delay before pressing "p" would accidentally paste).
" \l       : list buffers
" \b \f \g : go back/forward/last-used
" \d       : erase buffer
" \1 \2 \3 : go to buffer 1/2/3 etc
nnoremap <Leader>l :ls<CR>
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>f :bn<CR>
nnoremap <Leader>d :bd<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

" TagBar
nmap <C-T> :TagbarToggle<CR>

" FZF
nmap <C-F> :Files<CR>

" myvimgrep
nmap <leader>s :Gr <c-r>=expand("<cword>")<cr><cr>

" Vim-fugitive settings
nmap <leader>gsp :Gsplit<cr>
nmap <leader>gvsp :Gvsplit<cr>
nmap <leader>gd :Gdiff<cr>
nmap <leader>gb :Gblame<cr>
nmap <leader>gs :Gstatus<cr>
nmap <leader>gc :Git commit -m ""
nmap <leader>ga :Git add .<cr>
nmap <leader>gp :Git push origin
