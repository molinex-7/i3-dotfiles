"******************************************************************************
" Func to generate our CTags
"******************************************************************************

function! GenerateCtags (...)
    if a:0 > 0
        silent exec "!~/.vim/scripts/shellscripts/generate_ctags.sh ".a:1
    else
        silent exec "!~/.vim/scripts/shellscripts/generate_ctags.sh"
    endif

    set tags=.ctags/dinamic_tags
    set tags+=.ctags/static_tags
endfunction

autocmd BufRead,BufEnter,BufNewFile *.cc,*.cpp,*.h,*.hpp call GenerateCtags()
