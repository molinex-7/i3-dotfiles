#!/usr/bin/env bash

$(find . -type f -executable -exec file {} \; \
    | grep -Ew "ELF" \
    | head -n1 \
    | cut -d":" -f1)
