#!/usr/bin/env bash

generateStaticTags() {
    cat tags_* > static_tags
}

generateStdTags() {
    if [[ ! -d cpp_src ]]; then
        if [[ -f cpp_src.tar.bz2 ]]; then
            tar -xvf cpp_src.tar.bz2 > /dev/null
        else
            curl -s "https://www.vim.org/scripts/download_script.php?src_id=9178" \
                -o cpp_src.tar.bz2 \
                && tar -xvf cpp_src.tar.bz2 > /dev/null
        fi
    fi

    if [[ ! -f tags_cpp ]]; then
        ctags -R \
            --sort=1 \
            --c++-kinds=+p \
            --fields=+iaS \
            --extras=+q \
            --language-force=C++ \
            -f tags_cpp cpp_src

        generateStaticTags
    fi
}

generateLibTags() {
    if [[ ! -z "$1" ]]; then
        if [[ -d "/usr/include/$1" ]]; then
            if [[ ! -f "tags_$1" ]]; then
                ctags -R \
                    --sort=1 \
                    --c++-kinds=+p \
                    --fields=+iaS \
                    --extras=+q \
                    --language-force=C++ \
                    -f "tags_$1" \
                    "/usr/include/$1"
            fi
        elif [[ $(find /usr/include -type f -name $1) ]]; then
            if [[ ! -f "tags_$1" ]]; then
                find /usr/include -type f -name $1 \
                    | ctags -L- \
                    --sort=1 \
                    --c++-kinds=+p \
                    --fields=+iaS \
                    --extras=+q \
                    --language-force=C++ \
                    --tag-relative=no \
                    -f "tags_$1"
            fi
        fi

        generateStaticTags
    fi
}

generateDinamicTags() {
    if [[ $(find ./src ./hdr -type f) ]]; then
        find $(pwd)/src ./hdr -type f \
            | ctags -L- \
            --sort=1 \
            --c++-kinds=+p \
            --fields=+iaS \
            --extras=+q \
            --language-force=C++ \
            --tag-relative=no \
            -f .ctags/dinamic_tags
    fi
}

main() {
    if [[ -d hdr && -d src ]]; then
        [[ ! -d $(pwd)/.ctags ]] && mkdir $(pwd)/.ctags

        generateDinamicTags

        cd $(pwd)/.ctags

        generateStdTags
        generateLibTags "$1"
    fi
}

main "$1"
