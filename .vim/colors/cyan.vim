highlight clear

if exists("syntax_on")
  syntax reset
endif

set t_Co=256
let g:colors_name = "cyan"

hi ColorColumn      ctermfg=14 				ctermbg=0
hi Comment 			ctermfg=240
hi Constant 		ctermfg=1
hi CursorLine 		cterm=none
hi CursorLineNr 	cterm=bold 				ctermfg=0 				ctermbg=6
hi Directory 		ctermfg=6
hi Identifier 		ctermfg=6
hi LineNr 			ctermfg=14
hi MatchParen 		cterm=bold 				ctermfg=240 			ctermbg=none
hi MoreMsg 			ctermfg=14
hi NonText 			ctermfg=14
hi Normal 			ctermfg=14 				ctermbg=0
hi Operator 		ctermfg=14
hi PreProc 			ctermfg=6
hi Search 			ctermfg=0 				ctermbg=6
hi Special 			ctermfg=5
hi Statement 		cterm=none 				ctermfg=6
hi StatusLineTerm 	ctermfg=14 				ctermbg=0
hi StatusLineTermNC ctermfg=6 				ctermbg=0
hi StatusLine 	    ctermfg=0 				ctermbg=14
hi StatusLineNC 	ctermfg=0				ctermbg=240
hi String 			ctermfg=1
hi Todo 			ctermfg=0 				ctermbg=6
hi Type 			ctermfg=6
hi Visual 			term=reverse 			ctermfg=0 				ctermbg=14
hi VertSplit 		cterm=none 				ctermfg=14 				ctermbg=NONE
hi SignColumn       ctermfg=14              ctermbg=NONE
